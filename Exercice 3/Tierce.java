package TP5;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class Tierce extends JFrame implements ActionListener, Arbitre,Runnable {
	private Container panneau;
	private JLabel label1=new JLabel("R�sultat:");
	private	JButton go=new JButton("GO");
	private String[] resultat=new String[6];
	private Cheval[] chevaliste; 
	
	public Tierce(){
		setSize(400,300);
		setLocation(120,120);
		setTitle("Le Tierc�");
		panneau=getContentPane();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		panneau.add(label1,BorderLayout.NORTH);
		panneau.add(go,BorderLayout.SOUTH);

		go.addActionListener(this);

		setVisible(true);
		chevaliste=new Cheval[6];
		
	}


	public void actionPerformed(ActionEvent e) {

		Thread processus = new Thread(this);
		processus.start();
		
		
	}



	public void arrive(String id) {
		System.out.println(id);

		label1.setText(id);
	
	
		
			
	}


	@Override
	public void run() {
		// TODO Auto-generated method stub
		for(int j=0;j<6;j++){
			chevaliste[j]=new Cheval("Cheval"+j,50,this);
		

		}
		for(int i=0;i<6;i++){
			chevaliste[i].start();
		}
	}

}
